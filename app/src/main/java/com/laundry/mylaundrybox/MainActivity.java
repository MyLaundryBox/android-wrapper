package com.laundry.mylaundrybox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.net.Uri;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebChromeClient;
import android.webkit.JavascriptInterface;
import android.webkit.WebViewClient;
import android.webkit.ValueCallback;
import android.content.Intent;
import android.util.Log;
import android.app.ProgressDialog;
import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import com.parse.Parse;
import com.parse.ParseCrashReporting;

import java.math.BigDecimal;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private WebView webView;
    private ProgressDialog progressDialog;

    private PayPalConfiguration config;

    private CallbackManager callbackManager;
    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE=1001;
    private static final String TAG = MainActivity.class.getSimpleName();
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private final Double MINIMUM_TOP_UP_AMOUNT = 50.0;
    private final String DIRECT_PAYMENT_TOP_UP_DESCRIPTION = "Top up Credits";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isNetworkConnected()) {
            handleNoNetwork();
        }
        else {
            initializeParseCrashReporting();
            initializePayPal();
            initializeFacebook();

            setContentView(R.layout.activity_main);
            progressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            webView = (WebView) findViewById(R.id.webview);
            setWebViewSetting();
            webView.loadUrl(getString(R.string.host_url));
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isNetworkConnected()) {
            handleNoNetwork();
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("File", "before");
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("File", "start activity result");
        if(requestCode==FILECHOOSER_RESULTCODE) {
            Log.i("File", Integer.toString(requestCode));
            if (null == mUploadMessage && mFilePathCallback == null) return;

            Log.i("File", "pass");
            if (null != mUploadMessage) {
                Log.i("File", "pass 1");
                Uri result = null;
                if(resultCode == MainActivity.RESULT_OK) {
                    if (data == null && mCameraPhotoPath != null) {
                        //Log.i("url", mCameraPhotoPath);
                        result = Uri.parse(mCameraPhotoPath);
                    } else {
                        //Log.i("url", data.getDataString());
                        //result = data.getData();
                        result = Uri.parse(data.getDataString());
                    }
                }

                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            } else if (mFilePathCallback != null) {
                Log.i("File", "pass 2");
                Uri[] results = null;
                // Check that the response is a good one
                if(resultCode == MainActivity.RESULT_OK) {
                    if(data == null) {
                        // If there is not data, then we may have taken a photo
                        if(mCameraPhotoPath != null) {
                            results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }

                mFilePathCallback.onReceiveValue(results);
                mFilePathCallback = null;
            }
        } else if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            // register callback for facebook
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        else if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

            // try paypal if not found means its facebook callback
            if (confirm != null) {
                try {
                    Log.i("Paypal Callback", confirm.toJSONObject().toString(4));

                    String paypalID = confirm.toJSONObject().getJSONObject("response").getString("id");
                    Log.i("Paypal Transaction ID", paypalID);

                    String redirectPrefix = "";
                    String successUrl = "";

                    if(requestCode == 0) {
                        // TOP_UP_ACTION
                        redirectPrefix = "credit_topup";
                        successUrl = String.format("%s/#/content/%s?success=true&paypal_id=%s", getString(R.string.host_url), redirectPrefix, paypalID);
                    }
                    else {
                        // PAY_ORDER -> requestCode will be the OrderID
                        redirectPrefix = "order_record";
                        String orderID =  String.valueOf(requestCode);
                        successUrl = String.format("%s/#/content/%s?success=true&paypal_id=%s&order_id=%s", getString(R.string.host_url), redirectPrefix, paypalID, orderID);
                    }

                    webView.loadUrl(successUrl);

                } catch (JSONException e) {
                    Log.e("Paypal Callback", "an extremely unlikely failure occurred: ", e);
                }
            }
            else {
                // register callback for facebook
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }

        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("Result Callback", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("Paypal Callback", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    private void setWebViewSetting() {
        webView.setVisibility(View.INVISIBLE);
        webView.addJavascriptInterface(new NativeHelper(), "nativeHelper");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                view.loadUrl("javascript:window.onhashchange = function(e) { nativeHelper.checkUrl(e.newURL); };");
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if( url.startsWith("http:") || url.startsWith("https:") ) {
                    return false;
                }

                // Otherwise allow the OS to handle it
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
        });

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        //webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        //webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        /*try {
            java.lang.reflect.Method m = Class.forName("android.webkit.CacheManager").getDeclaredMethod("setCacheDisabled", boolean.class);
            m.setAccessible(true);
            m.invoke(null, true);
        } catch (Throwable e) {
            Log.i("myapp", "Reflection failed", e);
        }*/

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                /*Log.d("MyApplication", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);*/
            }

            //The undocumented magic method override
            //Eclipse will swear at you if you try to put @Override here
            // For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                Log.i("file", "2.0");
                mUploadMessage = uploadMsg;
                //Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                //i.addCategory(Intent.CATEGORY_OPENABLE);
                //i.setType("image/*");
                //MainActivity.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
                createIntentToTakePicture();
            }

            // For Android 3.0+
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                Log.i("file", "3.0");
                mUploadMessage = uploadMsg;
                //Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                //i.addCategory(Intent.CATEGORY_OPENABLE);
                //i.setType("*/*");
                //MainActivity.this.startActivityForResult(
                //        Intent.createChooser(i, "File Browser"),
                //        FILECHOOSER_RESULTCODE);*/
                createIntentToTakePicture();
            }

            //For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                Log.i("file", "4.0");
                mUploadMessage = uploadMsg;
                //Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                //i.addCategory(Intent.CATEGORY_OPENABLE);
                //i.setType("image/*");
                //MainActivity.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), MainActivity.FILECHOOSER_RESULTCODE);
                createIntentToTakePicture();
            }

            //For Android 5.0
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                Log.i("file", "5.0");

                if (mFilePathCallback != null) {
                    mFilePathCallback.onReceiveValue(null);
                }
                mFilePathCallback = filePathCallback;
                createIntentToTakePicture();

                return true;
            }
        });
    }

    private void createIntentToTakePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "Unable to create Image File", ex);
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCameraPhotoPath = "file://" + photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
            } else {
                takePictureIntent = null;
            }
        }

        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
        contentSelectionIntent.setType("image/*");

        Intent[] intentArray;
        if (takePictureIntent != null) {
            intentArray = new Intent[]{takePictureIntent};
        } else {
            intentArray = new Intent[0];
        }

        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

        startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    /**
     * More info this method can be found at
     * http://developer.android.com/training/camera/photobasics.html
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    private void handleNoNetwork() {
        Toast.makeText(this, "No Internet connection", Toast.LENGTH_LONG).show();
        finish(); //Calling this method to close this activity when internet is not available.
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    private void initializeParseCrashReporting() {
        try {
            // Enable Crash Reporting
            ParseCrashReporting.enable(this);

            // Setup Parse
            Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
        }
        catch (Exception e) {
            Log.e("Parse", e.getMessage());
        }
    }

    private void initializePayPal() {
        String paypalConfig = getString(R.string.is_debug).equals("true") ? PayPalConfiguration.ENVIRONMENT_SANDBOX : PayPalConfiguration.ENVIRONMENT_PRODUCTION;
        config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(paypalConfig).clientId(getString(R.string.paypal_client_id));
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    private void initializeFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void setLoaded() {
        new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        webView.setVisibility(View.VISIBLE);
                    }
                });
            }
        }.start();
    }

    private void facebookLogin(final boolean fromLogin) {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, java.util.Arrays.asList("public_profile", "email", "user_birthday"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        com.facebook.AccessToken accessToken = loginResult.getAccessToken();
                        getFacebookProfileInfos(accessToken, fromLogin);
                    }

                    @Override
                    public void onCancel() {
                        Log.d("FB cancel", "cancel");
                    }

                    @Override
                    public void onError(FacebookException e) {
                        Log.d("FB error", e.toString());
                    }
                }
        );
    }

    private void getFacebookProfileInfos(final com.facebook.AccessToken accessToken, final boolean fromLogin) {
        Log.i("token", accessToken.getToken());
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject json, GraphResponse response) {
                if(response.getError() != null) {
                    Log.d("FB", "error");
                } else {
                    Log.i("FB", "success");
                    String jsonresult = String.valueOf(json);
                    Log.i("JSON result", jsonresult);
                    redirectSuccessFacebookLogin(json, accessToken.getToken(), accessToken.getUserId(), fromLogin);

                    /*Log.i("email", json.getString("email"));
                    Log.i("id", json.getString("id"));
                    Log.i("firstname", json.getString("first_name"));
                    Log.i("lastname", json.getString("last_name"));*/
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, first_name, last_name, email, gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void redirectSuccessFacebookLogin(JSONObject json, String accessToken, String userID, boolean fromLogin) {
        try {
            String email = json.has("email") ? json.getString("email") : "";
            String name = json.has("name") ? json.getString("name") : "";
            String gender = json.has("gender") ? json.getString("gender") : "";
            String dob = json.has("birthday") ? json.getString("birthday") : "";

            String prefix = fromLogin ? "login" : "register";
            String successUrl = getString(R.string.host_url) + "/#/content/" + prefix + "?email=" + email + "&name=" + name + "&gender=" + gender + "&dob=" + dob + "&guid=" + userID + "&fb_token=" + accessToken;
            Log.i("fb redirect url", successUrl);
            webView.loadUrl(successUrl);
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    private class NativeHelper {
        @JavascriptInterface
        public void checkUrl(String url)
        {
            Log.i("url", url);
            String[] components= url.split("#");

            if(components.length > 1) {

                String actionQuery = components[1];

                if(actionQuery.contains("document_ready")) {
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    setLoaded();
                                }
                            },
                            300);
                } else if(actionQuery.contains("login_fb")) {
                    facebookLogin(true);
                } else if(actionQuery.contains("register_fb")) {
                    facebookLogin(false);
                } else if(actionQuery.contains("direct_paypal_top_up")) {
                    String amount = actionQuery.split("=")[1];
                    topUpPay(amount);
                } else if(actionQuery.contains("direct_paypal_pay_order")) {
                    String queryParams = actionQuery.split("\\?")[1];
                    String[] paramComponents = queryParams.split("&");
                    String orderID = "";
                    String amount = "";
                    for(String param : paramComponents) {
                        String[] cmp = param.split("=");
                        if(param.contains("order_id")) {
                            Log.i("order_id", cmp[1]);
                            orderID = cmp[1];
                        } else if(param.contains("amount")) {
                            Log.i("amount", cmp[1]);
                            amount = cmp[1];
                        }
                    }
                    orderPay(amount, orderID);
                }
            }
        }
    }

    private void topUpPay(String amount) {

        // Amount must be more than a limit
        if(Float.parseFloat(amount) < MINIMUM_TOP_UP_AMOUNT) {
            return;
        }

        PayPalPayment payment = new PayPalPayment(
                new BigDecimal(amount),
                "SGD",
                DIRECT_PAYMENT_TOP_UP_DESCRIPTION,
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        // 0 -> TOP_UP ACTION
        startActivityForResult(intent, 0);
    }

    private void orderPay(String amount, String orderID) {

        if(amount == "") {
            return;
        }

        if(orderID == "") {
            return;
        }

        String description = String.format("Pay Order #%s", orderID);
        Log.i("Paypal Description", description);

        PayPalPayment payment = new PayPalPayment(
                new BigDecimal(amount),
                "SGD",
                description,
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        // send orderID to requestCode
        startActivityForResult(intent, Integer.valueOf(orderID));
    }
}
